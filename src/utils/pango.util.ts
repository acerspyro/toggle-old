import Pango from "gi://Pango";

/**
 * This function converts a Pango font size to points.
 *
 * It automatically handles absolute and relative sizes.
 * @param fontDescription The Pango font description
 * @returns a size in points as a positive integer
 */
export function convert_pango_font_size_to_points(
  fontDescription: Pango.FontDescription
) {
  if (fontDescription.get_size_is_absolute()) {
    return fontDescription.get_size();
  }

  return Math.round(fontDescription.get_size() / Pango.SCALE);
}
