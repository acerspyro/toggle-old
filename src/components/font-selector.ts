import Adw from "gi://Adw";
import GObject from "gi://GObject";
import Gtk from "gi://Gtk?version=4.0";
import Pango from "gi://Pango";
import Gio from "gi://Gio";
import { convert_pango_font_size_to_points } from "../utils/pango.util.js";

Gio._promisify(Gtk.FontDialog.prototype, "choose_font", "choose_font_finish");

export class FontSelectorComponent extends Adw.ActionRow {
  private _font_desc: Pango.FontDescription = new Pango.FontDescription();

  static {
    GObject.registerClass(
      {
        GTypeName: "FontSelectorComponent",
        Template: "resource:///app/drey/Toggle/ui/components/font-selector.ui",
        Properties: {
          "font-desc": GObject.ParamSpec.boxed(
            "font-desc",
            "Pango Font Description",
            "The selected font as a Pango Font Description",
            GObject.ParamFlags.READWRITE,
            Pango.FontDescription.$gtype
          ),
        },
      },
      this
    );
  }

  _init(params = {}) {
    super._init(params);

    this.connect("activated", async () => {
      const font_dialog = new Gtk.FontDialog();

      try {
        const new_font = (await font_dialog.choose_font(
          this.get_root() as unknown as Gtk.Window,
          this.font_desc,
          null
        )) as unknown as Pango.FontDescription;

        this.font_desc = new_font;
      } catch (error) {
        // Do nothing
      }
    });
  }

  set font_desc(font: Pango.FontDescription) {
    const fontSize = convert_pango_font_size_to_points(font);
    const text = `${font.get_family()} ${fontSize}`;
    this._font_desc = font;
    this.subtitle =
      `<span font-family='${font.get_family()}' ` +
      `style='${font.get_style()}' ` +
      `weight='${font.get_weight()}' ` +
      `variant='${font.get_variant()}' ` +
      `stretch='${font.get_stretch()}'>` +
      `${text}</span>`;
    this.notify("font-desc");
  }

  get font_desc() {
    return this._font_desc;
  }
}
